let playerOne,
    playerTwo,
    circleTurn, 
    columnTurn, 
    winner = 0,
    CROSS = 'cross',
    CIRCLE = 'circle'

const setTableHistory = () => {
    let data = JSON.parse(localStorage.getItem("history-game")) ?? [],
        tBody = []

    data.forEach((v, i) => {
        tBody.push(`
            <tr>
                <td style="text-align: center;">${i+1}</td>
                <td>${v.date}</td>
                <td>${v.player}</td>
                <td>${v.winner}</td>
            </tr>
        `)
    })

    document.getElementById('history-content').innerHTML = tBody.join('')
}

const setTurn = () => {
    let gas = document.getElementById('game-after-start'),
        gi = document.getElementById('game-info')

    gas.classList.remove('turn-player-one')
    gas.classList.remove('turn-player-two')
  
    if (circleTurn) {
        gi.innerHTML = playerTwo+' silahkan main'
        gi.style.color = "blue";
        gas.classList.add('turn-player-two')
    } else {
        gi.innerHTML = playerOne+' silahkan main'
        gi.style.color = "red";
        gas.classList.add('turn-player-one')
    }
}

const startGame = (col) => {
    circleTurn = false
    columnTurn = col
    winner = 0
    document.querySelectorAll('[data-cell]').forEach(cell => {
        cell.classList.remove(CROSS)
        cell.classList.remove(CIRCLE)
        cell.removeEventListener('click', handleClick)
        cell.addEventListener('click', handleClick, { once: true })
    })

    setTableHistory()
}

const handleClick = (e) => {
    const cell = e.target
    const currentClass = circleTurn ? CIRCLE : CROSS

    if (!winner) {
        placeMark(cell, currentClass)   
    }

    if (checkWin(currentClass)) {
        endGame(false)
    } else if (checkDraw()) {
        endGame(true)
    } else {
        swapTurns()
        setTurn()   
    }
}

const placeMark = (cell, currentClass) => {
    cell.classList.add(currentClass)
}

const swapTurns = () => {
    circleTurn = !circleTurn
}

const checkWin = (currentClass) => {
    let combination = []
        cellElements = document.querySelectorAll('[data-cell]')

    switch (parseInt(columnTurn)) {
        case 3:
            combination.push(
                [0, 1, 2], [3, 4, 5],
                [6, 7, 8], [0, 3, 6],
                [1, 4, 7], [2, 5, 8],
                [0, 4, 8], [2, 4, 6]
            )
            break;
        case 4:
            combination.push(
                [0, 1, 2], [1, 2, 3],
                [4, 5, 6], [5, 6, 7],
                [8, 9, 10], [9, 10, 11],
                [12, 13, 14], [13, 14, 15],
                [0, 4, 8], [8, 4, 12],
                [1, 5, 9], [5, 9, 13],
                [2, 6, 10], [6, 10, 14],
                [3, 7, 11], [7, 11, 15],
                [1, 6, 11], [0, 5, 10],
                [5, 10, 15], [4, 9, 14],
                [2, 5, 8], [3, 6, 9],
                [6, 9, 12], [7, 10, 13]
            )
            break;
        case 5:
            combination.push(
                [0, 1, 2], [1, 2, 3], [2, 3, 4],
                [5, 6, 7], [6, 7, 8], [7, 8, 9],
                [10, 11, 12], [11, 12, 13], [12, 13, 14],
                [15, 16, 17], [16, 17, 18], [17, 18, 19],
                [20, 21, 22], [21, 22, 23], [22, 23, 24],
                [0, 5, 10], [5, 10, 15], [10, 15, 20],
                [1, 6, 11], [6, 11, 16], [11, 16, 21],
                [2, 7, 12], [7, 12, 17], [12, 17, 22],
                [3, 8, 13], [8, 13, 18], [13, 18, 23],
                [4, 9, 14], [9, 14, 19], [14, 19, 24],
                [2, 8, 14], [1, 7, 13], [7, 13, 19],
                [0, 6, 12], [6, 12, 18], [12, 18, 24],
                [5, 11, 17], [11, 17, 23], [10, 16, 22],
                [2, 6, 10], [3, 7, 11], [7, 11, 15],
                [4, 8, 12], [8, 12, 16], [12, 16, 20],
                [9, 13, 17], [13, 17, 21], [14, 18, 22]
            )
            break;
        case 6:
            combination.push(
                [0, 1, 2], [1, 2, 3], [2, 3, 4], [3, 4, 5],
                [6, 7, 8], [7, 8, 9], [8, 9, 10], [9, 10, 11],
                [12, 13, 14], [13, 14, 15], [14, 15, 16], [15, 16, 17],
                [18, 19, 20], [19, 20, 21], [20, 21, 22], [21, 22, 23],
                [24, 25, 26], [25, 26, 27], [26, 27, 28], [27, 28, 29],
                [30, 31, 32], [31, 32, 33], [32, 33, 34], [33, 34, 35],
                [0, 6, 12], [6, 12, 18], [12, 18, 24], [18, 24, 30],
                [1, 7, 13], [7, 13, 19], [13, 19, 25], [19, 25, 31],
                [2, 8, 14], [8, 14, 20], [14, 20, 26], [20, 26, 32],
                [3, 9, 15], [9, 15, 21], [15, 21, 27], [21, 27, 33],
                [4, 10, 16], [10, 16, 22], [16, 22, 28], [22, 28, 34],
                [5, 11, 17], [11, 17, 23], [17, 23, 29], [23, 29, 35],
                [3, 10, 17], [2, 9, 16], [9, 16, 32],
                [1, 8, 15], [8, 15, 22], [15, 22, 29],
                [0, 7, 14], [7, 14, 21], [14, 21, 18],
                [21, 28, 35], [6, 13, 20], [13, 20, 27],
                [20, 27, 34], [12, 19, 26], [19, 26, 33],
                [18, 25, 32], [2, 7, 12], [3, 18, 13],
                [8, 13, 18], [4, 19, 14], [9, 14, 19],
                [14, 19, 24], [5, 10, 15], [10, 15, 20],
                [15, 20, 25], [20, 25, 30], [11, 16, 21],
                [16, 21, 26], [21, 26, 31], [17, 22, 27],
                [22, 27, 32], [23, 28, 33]
            )
            break;
    
        default:
            break;
    }

    if (combination.length) {
        return combination.some(com => {
            return com.every(index => {
                return cellElements[index].classList.contains(currentClass)
            })
        })   
    }
}

const checkDraw = () => {
    let cellElements = document.querySelectorAll('[data-cell]')

    return [...cellElements].every(cell => {
      return cell.classList.contains(CROSS) || cell.classList.contains(CIRCLE)
    })
}

const pushHistory = (historyGame, draw) => {
    let today = new Date(),
        day = today.getDate(),
        month = today.getMonth() + 1,
        year = today.getFullYear()

    historyGame.maxlength = 10;
    historyGame.push = function(elem) {
        if(this.length == this.maxlength) {
            this.pop();
        }
        return [].unshift.call(this, elem);
    }
    historyGame.push({
        date: day + "/" + month + "/" + year + ' '+today.getHours()+':'+today.getMinutes()+':'+today.getSeconds(),
        player: playerOne + ' vs ' + playerTwo,
        winner: draw ? '-' : (circleTurn ? playerTwo: playerOne)
    })

    localStorage.setItem("history-game", JSON.stringify(historyGame));
    setTableHistory()
}

const endGame = (draw) => {
    let historyGame = JSON.parse(localStorage.getItem("history-game")) ?? [],
        gi = document.getElementById('game-info')
       
    if (draw) {
        winner = 2
        gi.innerHTML = 'Permainan seri !!!'
        gi.style.color = "black";

        pushHistory(historyGame, draw)
    } else {
        winner = 1
        gi.innerHTML = `Permainan dimenangkan oleh ${circleTurn ? playerTwo: playerOne}`
        gi.style.color = "green";

        pushHistory(historyGame, draw)
    }
}


// ---------------------------------------------------

document.getElementById('btn-mulai').addEventListener('click', function() {
    let p1 = document.getElementById('player-1'),
        p2 = document.getElementById('player-2'),
        bsi = document.getElementById('before-start-info'),
        gbs = document.getElementById('game-before-start'),
        gas = document.getElementById('game-after-start'),
        lc = document.getElementById('line-col'),
        setColumn = lc.options[lc.selectedIndex].value,
        cell = [], 
        setBorderLeft = 0, 
        setBorderRight = setColumn; 

    if (!p1.value.length) {
        bsi.innerHTML = 'Nama player 1 harus diisi'
        bsi.style.color = 'red'
    } else if (!p2.value.length) {
        bsi.innerHTML = 'Nama player 2 harus diisi'
        bsi.style.color = 'red'
    } else {
        bsi.innerHTML = ''
        bsi.style.color = 'black'
        gbs.style.display = 'none'
        gas.style.display = 'block'
        gas.innerHTML = 'Loading ...'

        setTimeout(() => {
            for (let index = 0; index < setColumn*setColumn; index++) {
                let borderTop = '', borderLeft = '', borderRight = '', borderBottom = ''
        
                if (index <= parseInt(setColumn)-1) {
                    borderTop = ' border-top: none;' 
                } else {
                    if (index > (parseInt(setColumn)*parseInt(setColumn))-parseInt(setColumn)-1) {
                        borderBottom = ' border-bottom: none;'
                    }
                }
        
                if (setBorderLeft == index) {
                    setBorderLeft = parseInt(setBorderLeft)+parseInt(setColumn)
                    borderLeft = ' border-left: none;'
                }
        
                if (parseInt(setBorderRight)-1 == index) {
                    setBorderRight = parseInt(setBorderRight)+parseInt(setColumn)
                    borderRight = ' border-right: none;'
                }
        
                cell.push(`<div class="cell" style="width: 50px; height: 50px;${borderTop+borderLeft+borderRight+borderBottom}" data-cell>${index}</div>`) 
            }
        
            playerOne = p1.value
            playerTwo = p2.value
            gas.style = `display: grid; grid-template-columns: repeat(${setColumn}, auto);`;
            gas.innerHTML = cell.join('')
            document.getElementById('btn-ulangi').style = 'display: block; margin-top: 20px'
            startGame(setColumn)
            setTurn()
        }, 1000);
    }
})

document.getElementById('btn-ulangi').addEventListener('click', function() {
    let gbs = document.getElementById('game-before-start'),
        gas = document.getElementById('game-after-start'),
        gi = document.getElementById('game-info')

    gbs.style.display = 'flex'
    gas.style.display = 'none'
    gas.innerHTML = ''
    gas.classList.remove('turn-player-one')
    gas.classList.remove('turn-player-two')
    document.getElementById('btn-ulangi').style.display = 'none'
    gi.innerHTML = 'Selamat datang ...'
    gi.style.color = 'black'
  
    startGame(0)
})

startGame(0)