<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="styles.css">
        <script src="script.js" defer></script>
        <title>Tic Tac Toe</title>
    </head>
    <body>
        <div id="wrapper">
            <h1>Tic Tac Toe</h1>

            <div id="content">
                <div id="game">
                    <div id="game-title">Table Permainan</div>

                    <div id="game-info">Selamat datang ...</div>

                    <div id="game-content">
                        <div id="game-before-start">
                            <div id="before-start-info"></div>
                            <input type="text" class="player-input" id="player-1" placeholder="Silahkan isi nama player 1">
                            <input type="text" class="player-input" id="player-2" placeholder="Silahkan isi nama player 2">
                            <div id="game-column">
                                <span>Jumlah baris / kolom</span>
                                <select id="line-col">
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </div>
                            <button id="btn-mulai">Mulai!</button>
                        </div>

                        <div id="game-after-start" style="display: none;"></div>
                        <button id="btn-ulangi" style="display: none;">Ulangi</button>
                    </div>
                </div>
    
                <div id="history">
                    <div id="history-title">Riwayat Permainan</div>

                    <table border="1" style="width: 100%; margin-top: 10px;">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tanggal</th>
                                <th>Pemain</th>
                                <th>Pemenang</th>
                            </tr>
                        </thead>
                        <tbody id="history-content">
                        </tbody>
                    </table>
                </div> 
            </div>
        </div>
    </body>
</html>